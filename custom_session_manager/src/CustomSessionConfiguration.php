<?php

namespace Drupal\custom_session_manager;

use Drupal\Core\Session\SessionConfiguration;
use Symfony\Component\HttpFoundation\Request;

/**
 * Sets session and cookie lifetime dynamically.
 */
class CustomSessionConfiguration extends SessionConfiguration {

  /**
   * {@inheritdoc}
   */
  public function getOptions(Request $request) {
    $options = parent::getOptions($request);

    // Allows the cookie to be destroyed when closing browser.
    $options['cookie_lifetime'] = 0;
    
    // Put the session available for the Garbage Collector.
    $options['gc_maxlifetime'] = 1800;

    return $options;
  }

}